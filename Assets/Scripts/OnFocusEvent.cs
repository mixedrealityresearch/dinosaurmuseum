﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;
using UnityEngine.Events;

public class OnFocusEvent : MonoBehaviour, IFocusable
{
    public UnityEvent FocusEnterEvent;
    public UnityEvent FocusLostEvent;

    public void OnFocusEnter()
    {
        if (FocusEnterEvent != null)
        {
            FocusEnterEvent.Invoke();
        }
        //throw new System.NotImplementedException();
    }

    public void OnFocusExit()
    {
        if (FocusLostEvent != null)
        {
            FocusLostEvent.Invoke();
        }
        //throw new System.NotImplementedException();
    }

    
}

