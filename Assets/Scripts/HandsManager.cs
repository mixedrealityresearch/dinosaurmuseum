﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.WSA.Input;

namespace HoloToolkit.Unity
{
    /// <summary>
    /// HandsDetected determines if the hand is currently detected or not.
    /// </summary>
    public partial class HandsManager : Singleton<HandsManager>
    {

        private List<uint> trackedHands = new List<uint>();

        /// <summary>
        /// HandDetected tracks the hand detected state.
        /// </summary>
        public bool HandDetected {
            get { return trackedHands.Count > 0; }
        }

        private void Awake()
        {
            InteractionManager.InteractionSourceDetected += InteractionManager_SourceDetected;
            InteractionManager.InteractionSourceLost += InteractionManager_SourceLost;
        }

        private void InteractionManager_SourceDetected(InteractionSourceDetectedEventArgs args)
        {
            // Check to see that the source is a hand
            if (args.state.source.kind != InteractionSourceKind.Hand)
            {
                return;
            }

            trackedHands.Add(args.state.source.id);
        }

        private void InteractionManager_SourceLost(InteractionSourceLostEventArgs args)
        {
            // Check to see that the source is a hand
            if (args.state.source.kind != InteractionSourceKind.Hand)
            {
                return;
            }

            if (trackedHands.Contains(args.state.source.id))
            {
                trackedHands.Remove(args.state.source.id);
            }
        }

        private void OnDestroy()
        {
            InteractionManager.InteractionSourceDetected -= InteractionManager_SourceDetected;
            InteractionManager.InteractionSourceLost -= InteractionManager_SourceLost;
        }
    }
}