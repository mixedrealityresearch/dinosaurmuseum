﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TyrannoControl : MonoBehaviour {

    public ARAI_IntelligenManager IntelligentManager;

    [Tooltip("Drag the Communicator prefab asset.")]
    public GameObject CommunicatorPrefab;
    private GameObject communicatorGameObject;

    [Tooltip("Drag the Voice Tooltip prefab asset.")]
    public GameObject OpenCommunicatorTooltip;
    private GameObject openCommunicatorTooltipGameObject;

    private SpeechInputSource speechInputSource;
    private DictationInputManager dictationManager;

    private void Awake()
    {
        // Tooltip
        openCommunicatorTooltipGameObject = Instantiate(OpenCommunicatorTooltip);
        openCommunicatorTooltipGameObject.transform.position = new Vector3(
            gameObject.transform.position.x - 1.0f,
            gameObject.transform.position.y + 0.8f,
            gameObject.transform.position.z - 0.8f);

        openCommunicatorTooltipGameObject.transform.parent = gameObject.transform;
        openCommunicatorTooltipGameObject.SetActive(false);

        speechInputSource = GetComponent<SpeechInputSource>();
    }
    // Use this for initialization
    void Start () {
        dictationManager = GetComponent<DictationInputManager>();
    }
	
	// Update is called once per frame
	void Update () {
    }

    // When users says 'Hey, ARAI', it will starts recording
    // in 5 seconds (changes this timeout in MicrophoneManager)
    public void StartRecording()
    {
        Debug.Log("Start recording with: " + this.gameObject.transform.name);
        communicatorGameObject = Instantiate(CommunicatorPrefab);
    }


}
