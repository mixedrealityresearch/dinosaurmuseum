﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CV
{
	public class FreeLookCamera : MonoBehaviour
	{
		public float lookSpeed = 200.0f;
		public float moveSpeed = 45.0f;
		public float zoomSpeed = 200.0f;
		private float rotationX;
		private float rotationY;

		void Start ()
		{
			rotationX = transform.localEulerAngles.x;
			rotationY = transform.localEulerAngles.y;
		}

	
		void Update ()
		{
			if (Input.GetMouseButton (1)) {
				rotationX -= Input.GetAxis ("Mouse Y") * lookSpeed * Time.deltaTime;
				rotationY += Input.GetAxis ("Mouse X") * lookSpeed * Time.deltaTime;

				transform.localRotation = Quaternion.Euler (new Vector3 (rotationX, rotationY, 0));
			}

			if (Input.GetAxis ("Mouse ScrollWheel") > 0) {
				transform.position += transform.forward * zoomSpeed * Time.deltaTime;
			}
			if (Input.GetAxis ("Mouse ScrollWheel") < 0) {
				transform.position -= transform.forward * zoomSpeed * Time.deltaTime;
			}
			var moveStepZ = transform.forward * moveSpeed * Input.GetAxis ("Vertical") * Time.deltaTime;
			var moveStepX = transform.right * moveSpeed * Input.GetAxis ("Horizontal") * Time.deltaTime;
			if (Input.GetKey (KeyCode.LeftShift)) {  
				moveStepX *= 2;
				moveStepZ *= 2;
			}
			transform.position += moveStepZ;
			transform.position += moveStepX;

		}
	}
}