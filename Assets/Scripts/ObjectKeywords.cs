﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class ObjectKeywords : MonoBehaviour, ISpeechHandler
{
    private Material cachedMaterial;
    private Color defaultColor;

    private void Awake()
    {
        cachedMaterial = GetComponent<Renderer>().material;
        defaultColor = cachedMaterial.color;
    }

    public void ChangeColor(string color)
    {
        switch (color.ToLower())
        {
            case "red":
                cachedMaterial.SetColor("_Color", Color.red);
                break;
            case "green":
                cachedMaterial.SetColor("_Color", Color.green);
                break;
            case "blue":
                cachedMaterial.SetColor("_Color", Color.blue);
                break;
        }
    }

    public void ResetColor()
    {
        cachedMaterial.SetColor("_Color", defaultColor);
    }

    public void GetObjectInfo()
    {
        Debug.Log("This function shows information of the GAME OBJECT is being gazed.");
        Debug.Log("Name: " + this.gameObject.transform.name);
        Debug.Log("Tag: " + this.gameObject.transform.tag);
        Debug.Log("Position: " + this.gameObject.transform.position);
    }

    public void OnSpeechKeywordRecognized(SpeechEventData eventData)
    {
        ChangeColor(eventData.RecognizedText);
        //throw new System.NotImplementedException();
    }

    private void OnDestroy()
    {
        DestroyImmediate(cachedMaterial);
    }
}
