﻿using System.Collections;
using UnityEngine;

public class BallController : MonoBehaviour {

    public GameObject ballObject;
    public int ballCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;

    // Use this for initialization
    void Start () {
        StartCoroutine(BallWaves());
	}

    IEnumerator BallWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < ballCount; i++)
            {
                //Vector3 ballPosition = new Vector3(Random.Range(-2.54f, 3.13f), Random.Range(0.92f, 2.62f), Random.Range(0.143f, 1.537f));
                Vector3 ballPosition = new Vector3(Random.Range(29.83f, 59.1f), Random.Range(11.6f, 17.49f), Random.Range(-32.13f, -3.17f));

                Quaternion ballRotation = Quaternion.identity;
                Instantiate(ballObject, ballPosition, ballRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);
        }
    }
}
