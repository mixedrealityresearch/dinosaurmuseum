﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Attack")]
public class AttackAction : ActionAI
{
    public override void Act(StateController controller)
    {
        Attack(controller);
    }

    private void Attack(StateController controller)
    {
        RaycastHit hit;

        Debug.DrawRay(controller.eyes.position, controller.eyes.forward.normalized * controller.enemyStats.attackRange, Color.red);

        if (Physics.SphereCast(controller.eyes.position, controller.enemyStats.lookSphereCastRadius, controller.eyes.forward, out hit, controller.enemyStats.attackRange)
            && hit.collider.CompareTag("Player"))
        {
            if (controller.CheckIfCountDownElapsed(controller.enemyStats.attackRate))
            {
                controller.tankShooting.Fire(controller.enemyStats.attackForce, controller.enemyStats.attackRate);
            }
        }

        //var fwd = controller.gameObject.transform.TransformDirection(Vector3.forward);
        //Debug.DrawRay(controller.gameObject.transform.position, fwd * controller.enemyStats.attackRange, Color.red);

        //if (Physics.Raycast(controller.gameObject.transform.position, fwd, out hit, controller.enemyStats.attackRange) && hit.collider.CompareTag("Player"))
        //{
        //    if (controller.CheckIfCountDownElapsed(controller.enemyStats.attackRate))
        //    {
        //        controller.tankShooting.Fire(controller.enemyStats.attackForce, controller.enemyStats.attackRate);
        //    }
        //}
    }
}
