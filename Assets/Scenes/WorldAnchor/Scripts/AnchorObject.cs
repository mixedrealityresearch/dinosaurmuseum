﻿using HoloToolkit.Unity;
using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnchorObject : MonoBehaviour, IManipulationHandler, IFocusable {

    public string AnchorName = "123456789";
    public bool Undo = false;

    private Vector3 SavedPosition;
    private Quaternion SavedRotation;
    private Vector3 OldPosition;
    private Quaternion OldRotation;

    // Use this for initialization
    void Start()
    {
        OldPosition = SavedPosition = this.gameObject.transform.position;
        OldRotation = SavedRotation = this.gameObject.transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        if (Undo)
        {
            WorldAnchorManager.Instance.RemoveAnchor(this.gameObject);
            RestoreBackupAnchor();
            Undo = false;
        }
    }

    public void RestoreBackupAnchor()
    {
        this.gameObject.transform.position = OldPosition;
        this.gameObject.transform.rotation = OldRotation;
        WorldAnchorManager.Instance.AttachAnchor(this.gameObject, AnchorName);
    }

    public void OnFocusEnter()
    {
        //throw new System.NotImplementedException();
    }

    public void OnFocusExit()
    {
        SavedPosition = this.gameObject.transform.position;
        SavedRotation = this.gameObject.transform.rotation;
    }

    public void AttachAnchor()
    {
        OldPosition = SavedPosition = this.gameObject.transform.position;
        OldRotation = SavedRotation = this.gameObject.transform.rotation;

        WorldAnchorManager.Instance.AttachAnchor(this.gameObject, AnchorName);
        Debug.Log("Anchor attached for: " + this.gameObject.name + " - Anchor ID:" + AnchorName);
    }

    public void UpdatePositionAndRemoveAnchor()
    {
        OldPosition = SavedPosition = this.gameObject.transform.position;
        OldRotation = SavedRotation = this.gameObject.transform.rotation;

        WorldAnchorManager.Instance.RemoveAnchor(this.gameObject);
    }

    public void OnManipulationCanceled(ManipulationEventData eventData)
    {
        //throw new System.NotImplementedException();
    }

    public void OnManipulationCompleted(ManipulationEventData eventData)
    {
        WorldAnchorManager.Instance.AttachAnchor(this.gameObject, AnchorName);
        Debug.Log("OnManipulationCompleted - Anchor Attached.");
    }

    public void OnManipulationStarted(ManipulationEventData eventData)
    {
        WorldAnchorManager.Instance.RemoveAnchor(this.gameObject);
        Debug.Log("OnManipulationStarted - Remove Anchor.");
        OldPosition = SavedPosition;
        OldRotation = SavedRotation;
    }

    public void OnManipulationUpdated(ManipulationEventData eventData)
    {
        //throw new System.NotImplementedException();
    }

    
}
