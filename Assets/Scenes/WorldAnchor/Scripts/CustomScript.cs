﻿using HoloToolkit.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.WSA;
using UnityEngine.XR.WSA.Persistence;

public class CustomScript : MonoBehaviour {

    float speed = 2f;
    Vector3 targetPosition = new Vector3(1.92f, 0.4f , -3.08f);
    public GameObject gameObject;

    string anchorName = "anchor_1";

    void Start () {

        Debug.Log("Start: " + gameObject.transform.position);
        Debug.Log("Adding anchor initially");

        // Adding anchor initially
        WorldAnchorManager.Instance.AttachAnchor(gameObject, anchorName);

    }

    private void Update()
    {
        WorldAnchorManager.Instance.RemoveAnchor(gameObject);

        float step = speed * Time.deltaTime;
        gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, targetPosition, step);

    }

    private void OnCollisionExit(Collision collision)
    {
        Debug.Log("OnCollisionExit");
        Debug.Log("After moving: " + gameObject.transform.position);

        Debug.Log("Now saving position of cylinder.");

        WorldAnchorManager.Instance.AttachAnchor(gameObject, anchorName);

    }


}
