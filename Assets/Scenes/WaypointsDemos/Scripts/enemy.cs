﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy : MonoBehaviour {

    public Transform[] target;
    public float speed = 10.0f;
    private int current;

	// Update is called once per frame
	void Update () {

        // Move until you reach the current object/waypoint
		if (this.transform.position != target[current].position)
        {
            Vector3 pos = Vector3.MoveTowards(this.transform.position, target[current].position, speed*Time.deltaTime);
            GetComponent<Rigidbody>().MovePosition(pos);
        }
        else // object/waypoint reached, move to the next object
        {
            current = (current + 1) % target.Length;
        }
	}
}
