﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoints : MonoBehaviour {

    [SerializeField]
    protected float debugDrawRadius = 0.1f;

	public virtual void OnDrawGizmos () {

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(this.transform.position, debugDrawRadius);

	}
	
}
