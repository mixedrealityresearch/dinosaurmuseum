﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionAnswerDictionary : MonoBehaviour {

    class qa
    {

        public string ques;
        public string answer;
        public string emotion;

    };

    List<qa> qas;// = new List<qa>();
    public QuestionAnswerDictionary()
    {
        qas = new List<qa>();
        qa qi;


        qi = new qa();
        qi.ques = "What is your name?";
        qi.answer = "My name is Dinosaur.";
        qi.emotion = null;
        qas.Add(qi);

        qi = new qa();
        qi.ques = "What is your age?";
        qi.answer = "my age is two months";
        qi.emotion = null;
        qas.Add(qi);

        qi = new qa();
        qi.ques = "How are you today?";
        qi.answer = "I'm fine. Thank you.";
        qi.emotion = "Happy";
        qas.Add(qi);

        qi = new qa();
        qi.ques = "What's the meaning of your name?";
        qi.answer = "I don't know.";
        qi.emotion = "Sad";
        qas.Add(qi);

        qi = new qa();
        qi.ques = "Where you live?";
        qi.answer = "I live in Jungle";
        qi.emotion = null;
        qas.Add(qi);

        qi = new qa();
        qi.ques = "What did you eat for lunch?";
        qi.answer = "I eat green leaves for lunch";
        qi.emotion = "Happy";
        qas.Add(qi);

        qi = new qa();
        qi.ques = "Do you miss your family?";
        qi.answer = "Yes I miss my family.";
        qi.emotion = "Sad";
        qas.Add(qi);

        qi = new qa();
        qi.ques = "Where is your family?";
        qi.answer = "They were extinct millions of years ago.";
        qi.emotion = "Sad";
        qas.Add(qi);

        qi = new qa();
        qi.ques = "Do you have Friends?";
        qi.answer = "No my friends extinct millions of years ago.";
        qi.emotion = "Sad";
        qas.Add(qi);

        qi = new qa();
        qi.ques = "Tell me a joke?";
        qi.answer = "A conference call is the best way for a dozen people to say bye 300 times";
        qi.emotion = "Happy";
        qas.Add(qi);

        qi = new qa();
        qi.ques = "Say something romantic";
        qi.answer = "if you were a dinosaur, you'd be a Gorgeousaurus";
        qi.emotion = "Happy";
        qas.Add(qi);

        qi = new qa();
        qi.ques = "Are you angry";
        qi.answer = "No I am not angry";
        qi.emotion = "Yell";
        qas.Add(qi);

        qi = new qa();
        qi.ques = "You are Beautiful";
        qi.answer = "Really. are you flirting";
        qi.emotion = "Surprised";
        qas.Add(qi);

        qi = new qa();
        qi.ques = "Do you Like to play games?";
        qi.answer = "Yes, I like games very much";
        qi.emotion = "Happy";
        qas.Add(qi);

        qi = new qa();
        qi.ques = "How can you help me?";
        qi.answer = "Ask me what you need to know";
        qi.emotion = null;
        qas.Add(qi);

        qi = new qa();
        qi.ques = "Are you real?";
        qi.answer = "I am a hologram";
        qi.emotion = null;
        qas.Add(qi);

        qi = new qa();
        qi.ques = "are you stupid?";
        qi.answer = "No, I am not";
        qi.emotion = "Yell";
        qas.Add(qi);

        qi = new qa();
        qi.ques = "Are you brave?";
        qi.answer = "Yes, I am brave.";
        qi.emotion = null;
        qas.Add(qi);

        qi = new qa();
        qi.ques = "What are you doing?";
        qi.answer = "I am talking to you.";
        qi.emotion = "null.";
        qas.Add(qi);

        qi = new qa();
        qi.ques = "Do you like me?";
        qi.answer = "yes, I like you";
        qi.emotion = "Happy";
        qas.Add(qi);

        qi = new qa();
        qi.ques = "Can you sing me a song.";
        qi.answer = "My voice is not pretty";
        qi.emotion = "Sad";
        qas.Add(qi);

        qi = new qa();
        qi.ques = "What are your hobbies?";
        qi.answer = "To play and roam around";
        qi.emotion = "Happy";
        qas.Add(qi);

        qi = new qa();
        qi.ques = "Do you have a job.";
        qi.answer = "I am a dinosaur. Why I need a job";
        qi.emotion = "Surprised";
        qas.Add(qi);

        qi = new qa();
        qi.ques = "What is your Weight.";
        qi.answer = "I am 100 kg";
        qi.emotion = null;
        qas.Add(qi);

        qi = new qa();
        qi.ques = "You are Hot.";
        qi.answer = "Am I on fire";
        qi.emotion = "Surprised";
        qas.Add(qi);

        qi = new qa();
        qi.ques = "Tell me a science fact";
        qi.answer = "A person who studies dinosaurs is known as a paleontologist";
        qi.emotion = null;
        qas.Add(qi);

        qi = new qa();
        qi.ques = "Tell me about yourself";
        qi.answer = "I am dinosaur and I lived about 65 million years ago";
        qi.emotion = null;
        qas.Add(qi);
    }


    public string[] GetAnswers(string qu)
    {
        int d = 0;
        int minD = 1000000000;
        //string Answer = null;
        //an a = new an();
        string[] st = new string[2];
        foreach (qa q in qas)
        {
            d = dist(qu, q.ques);
            if (d < minD)
            {
                minD = d;
                st[0] = q.answer;
                st[1] = q.emotion;
                //Answer = q.answer;
            }

        }


        return st;
    }


    public int dist(string s, string t)
    {
        int n = s.Length;
        int m = t.Length;
        int[,] d = new int[n + 1, m + 1];
        if (n == 0)
        {
            return m;
        }
        if (m == 0)
        {
            return n;
        }
        for (int i = 0; i <= n; d[i, 0] = i++)
            ;
        for (int j = 0; j <= m; d[0, j] = j++)
            ;
        for (int i = 1; i <= n; i++)
        {
            for (int j = 1; j <= m; j++)
            {
                int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;
                d[i, j] = Math.Min(
                    Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                    d[i - 1, j - 1] + cost);
            }
        }
        return d[n, m];
    }
}
