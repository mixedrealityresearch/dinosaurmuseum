﻿using HoloToolkit.Unity.InputModule;
using System.Collections;
using UnityEngine;

public class DinosaurManager : MonoBehaviour {

    string[] _emotionL = new string[] {"Angry","Happy","Sad","Surprise"};

    private ARAI_IntelligenEntity intelligenEntity;

    [Tooltip("Drag the ARAI_IntelligenManager gameobject.")]
    public ARAI_IntelligenManager intelligentManager;

    Vector3 originalDinoPos;
    Vector3 currentDinoPos;
    Vector3 currentObjectPos;
    Vector3 targetPos;

    GameObject objectToMove = null;
    bool isObjectFocused = false;
    bool isMoved = false;
    bool isCalled = false;

    private void Start()
    {
        originalDinoPos = this.gameObject.transform.position;
        targetPos = new Vector3();
    }

    private void Awake()
    {
        intelligenEntity = GetComponent<ARAI_IntelligenEntity>();
    }

    private void Update()
    {
        if (GazeManager.Instance.HitObject != null)
        {
            if (GazeManager.Instance.HitObject.transform.tag == "go")
            {
                isObjectFocused = true;
                currentObjectPos = GazeManager.Instance.HitObject.transform.position;
                objectToMove = GazeManager.Instance.HitObject;
            }
            else if (isCalled && GazeManager.Instance.HitObject.transform.tag == "plane")
            {
                intelligentManager.MoveToPositionGazedObject(this.gameObject, GazeManager.Instance.HitPosition);
            }
        }

    }

    /// <summary>
    /// ComebackOrginalPosition is called when you says 'Come back'
    /// to the character which is being gazed.
    /// The character will come back its original position.
    /// </summary>
    public void ComebackOrginalPosition()
    {
        var currPos = transform.position;
        intelligentManager.MoveToPositionGazedObject(this.gameObject, originalDinoPos);
    }

    /// <summary>
    /// When user says 'Come to me', all dinosaurs will come closely
    /// to user.
    /// </summary>
    public void ComeToMe()
    {
        var currPos = transform.position;
        intelligentManager.MoveToPositionGazedObject(this.gameObject, originalDinoPos);
    }

    /// <summary>
    /// This function is called when you ask the character to get something
    /// in the scene. The character will get any object you are pointing and
    /// come back his original position.
    /// </summary>
    public void GetObject()
    {
        // check the current gazed object
        if (isObjectFocused)
        {
            currentDinoPos = this.gameObject.transform.position;

            // move the dino to the object is being gazed
            intelligentManager.MoveToPositionGazedObject(this.gameObject, currentObjectPos);

            // set the dino is moved
            isMoved = true;

            StartCoroutine(MoveDIno());
        }
    }

    /// <summary>
    /// This function allows the character brings the object when he tries
    /// to get something in the scene. This function is called in the Update().
    /// </summary>
    IEnumerator MoveObject(Transform thisTransform, Vector3 startPos, Vector3 endPos, float time)
    {
        var i = 0.0f;
        var rate = 1.0f / time;
        while (i < 1.0f)
        {
            i += Time.deltaTime * rate;
            thisTransform.position = Vector3.Lerp(startPos, endPos, i);
            yield return null;
        }

    }

    IEnumerator MoveDIno()
    {
        bool isColided = false;

        while (!isColided)
        {
            Vector3 dist = this.gameObject.transform.position - currentObjectPos;

            if (dist.magnitude <= .3)
            {
                isColided = true;

            }
            yield return null;
        }

        // Move the dino back to the current position when it collides with the box
        intelligentManager.MoveToPositionGazedObject(this.gameObject, currentDinoPos);

        // Move the object along with the dino
        StartCoroutine(MoveObject(objectToMove.transform, objectToMove.transform.position, currentDinoPos, 3.0f));
    }

    /// <summary>
    /// This function is called when user says 'Show me your face'
    /// to the character which is being gazed. The character shows
    /// his emotion which randomly picked up from the predefined list.
    /// </summary>
    public void ShowFace()
    {
        // Pick the random emotion from the list
        // and show the emotion
        intelligenEntity.animator.Play(_emotionL.RandomItem(), -1, 0f);

        // Play sounds here

    }

    public void OnSpeechKeywordRecognized(SpeechEventData eventData)
    {
        Debug.Log("YOU SAY: " + eventData.RecognizedText.ToLower());
    }

}

public static class ArrayExtensions
{
    // This is an extension method. RandomItem() will now exist on all arrays.
    public static T RandomItem<T>(this T[] array)
    {
        return array[Random.Range(0, array.Length)];
    }
}
