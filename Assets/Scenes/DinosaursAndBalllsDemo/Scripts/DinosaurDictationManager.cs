﻿using HoloToolkit.Unity;
using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DinosaurDictationManager : MonoBehaviour, IInputClickHandler, IDictationHandler {

    [SerializeField]
    [Range(0.1f, 5f)]
    [Tooltip("The time length in seconds before dictation recognizer session ends due to lack of audio input in case there was no audio heard in the current session.")]
    private float initialSilenceTimeout = 5f;

    [SerializeField]
    [Range(5f, 60f)]
    [Tooltip("The time length in seconds before dictation recognizer session ends due to lack of audio input.")]
    private float autoSilenceTimeout = 5f;

    [SerializeField]
    [Range(1, 60)]
    [Tooltip("Length in seconds for the manager to listen.")]
    private int recordingTime = 5;

    [Tooltip("Drag the ARAI_IntelligenManager gameobject.")]
    public ARAI_IntelligenManager intelligentManager;

    private ARAI_IntelligenEntity intelligentEntity;

    [SerializeField]
    private TextMesh speechToTextOutput;

    private TextToSpeech textToSpeech;

    private bool isRecording;

    private bool isCompleted;

    private string _str;

    private void Awake()
    {
        textToSpeech = GetComponent<TextToSpeech>();
        intelligentEntity = GetComponent<ARAI_IntelligenEntity>();
    }

    private void Update()
    {
        if (isCompleted)
        {
            ARAIRepeat();
        }
    }

    public void OnDictationComplete(DictationEventData eventData)
    {
        isCompleted = true;
        _str = eventData.DictationResult;
        speechToTextOutput.text = eventData.DictationResult;
        Debug.Log("OnDictationComplete. " + eventData.DictationResult);
        StartCoroutine(DictationInputManager.StopRecording());

        // Reset textOutput
        speechToTextOutput.color = Color.white;
        speechToTextOutput.text = "Gaze on me and say 'Hello' to start talking";
    }

    public void OnDictationError(DictationEventData eventData)
    {
        isRecording = false;
        speechToTextOutput.color = Color.red;
        speechToTextOutput.text = eventData.DictationResult;
        Debug.Log("OnDictationError: " + eventData.DictationResult);
        Debug.Log("Stop Recording...");
        StartCoroutine(DictationInputManager.StopRecording());
    }

    public void OnDictationHypothesis(DictationEventData eventData)
    {
        isCompleted = false;
        speechToTextOutput.text = eventData.DictationResult;
    }

    public void OnDictationResult(DictationEventData eventData)
    {
        speechToTextOutput.text = eventData.DictationResult;
        Debug.Log("OnDictationResult. " + eventData.DictationResult);
    }

    /// <summary>
    /// TextToSpeechOnTap
    /// The dinosaur will react whenever user taps on him
    /// </summary>
    public void OnInputClicked(InputClickedEventData eventData)
    {
        // If we have a text to speech manager on the target object, say something.
        // This voice will appear to emanate from the object.
        if (textToSpeech != null && eventData.PressType == InteractionSourcePressInfo.Select)
        {
            // Create message
            string _name = this.gameObject.transform.name;
            _name = _name.Substring(7);
            var st = _name.Split(' ');
            var msg = "Hello! My name is " + st[0] + ". How can I help you?";

            // Show emotion
            intelligentEntity.animator.Play("Happy", -1, 0f);

            // Speak message
            textToSpeech.StartSpeaking(msg);

            // Mark the event as used, so it doesn't fall through to other handlers.
            eventData.Use();
        }
    }

    public void StartCommunication()
    {
        Debug.Log("StartCommunication FUNCTION.");

        isRecording = true;
        StartCoroutine(DictationInputManager.StartRecording(initialSilenceTimeout, autoSilenceTimeout, recordingTime));
        speechToTextOutput.color = Color.green;
    }

    void ARAIRepeat()
    {
        Debug.Log("isCompleted: " + isCompleted);

        Debug.Log("ARAI should repeat. The text is: " + _str);

        intelligentManager.intelligentTalkGazedObject(this.gameObject, _str);

        Debug.Log("textToSpeech: " + textToSpeech);

        if (null != textToSpeech)
        {
            textToSpeech.StartSpeaking(intelligentManager.answer);
        }

        isCompleted = false;
    }


}
