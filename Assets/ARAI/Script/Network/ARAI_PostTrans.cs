﻿// using UnityEngine;
// using UnityEngine.Networking;
// using System.Collections;
// using System.Collections.Generic;
// using Naviworks_Util;
   
// // 현재는 사용하지 않는 스크립트.
// // 영어를 웹에서 발음기호, 한글로 번역하여 가져온다.
// public class ARAI_PostTrans : MonoBehaviour
// {
//     private static ARAI_PostTrans _instance;
//     public static ARAI_PostTrans Instance
//     {
//         get
//         {
//             if (!_instance)
//             {
//                 _instance = (ARAI_PostTrans)GameObject.FindObjectOfType(typeof(ARAI_PostTrans));
//                 if (!_instance)
//                 {
//                     GameObject container = new GameObject();
//                     container.name = "ARAI_PostTrans";
//                     _instance = container.AddComponent(typeof(ARAI_PostTrans)) as ARAI_PostTrans;
//                 }
//             }
//             return _instance;
//         }
//     }
   
//     public IEnumerator PostData_IPA_Phoneme(ARAI_IntelligenEntity _targetObj)
//     {
//         WWWForm formData = new WWWForm();
//         formData.AddField("text_to_transcribe", _targetObj.m_originText);
//         formData.AddField("output_dialect", "am");
//         formData.AddField("output_style", "only_tr");
   
//         UnityWebRequest www = UnityWebRequest.Post("http://lingorado.com/ipa/", formData);
//         yield return www.SendWebRequest();
   
//         if (www.isNetworkError || www.isHttpError)
//         {
//             Debug.Log(www.error + www.responseCode);
//         }
//         else
//         {
//             string result = ARAI_Utill.IPA_PhonemeParse(www.downloadHandler.text, "transcribed_word" , "<br />");
   
//             // 번역에 실패하였을 경우.
//             if (result == null)
//                 result = ARAI_Utill.IPA_PhonemeParse(www.downloadHandler.text, "transcription_missing", "<br />");
   
//             yield return StartCoroutine(PostPhoneToKorean(_targetObj,result));
//         }
//     }
   
//     IEnumerator PostPhoneToKorean(ARAI_IntelligenEntity _targetObj, string _IPA_Phoneme)
//     {
//         WWWForm formData = new WWWForm();
//         formData.AddField("contents", "[" + _IPA_Phoneme + "]");
//         formData.AddField("firstinput", "OK");
   
//         UnityWebRequest www = UnityWebRequest.Post("http://www.ltool.net/phonetic-alphabet-pronunciation-key-to-korean-converter-in-korean.php", formData);
//         yield return www.SendWebRequest();
   
//         if (www.isNetworkError || www.isHttpError)
//         {
//             Debug.Log(www.error + www.responseCode);
//         }
//         else
//         {
//             string result = ARAI_Utill.IPA_PhonemeToKorean(www.downloadHandler.text, _IPA_Phoneme, "]");
//             Debug.Log("result Korean" + _targetObj.m_originText);
   
//             yield return result;
//         }
//     }
// }