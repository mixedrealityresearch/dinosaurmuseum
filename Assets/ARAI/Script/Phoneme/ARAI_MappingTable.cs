﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARAI_MappingTable : MonoBehaviour
{
    private static ARAI_MappingTable _instance;
    public static ARAI_MappingTable Instance
    {
        get
        {
            if (!_instance)
            {
                _instance = (ARAI_MappingTable)GameObject.FindObjectOfType(typeof(ARAI_MappingTable));
                if (!_instance)
                {
                    GameObject container = new GameObject();
                    container.name = "ARAI_MappingTable";
                    _instance = container.AddComponent(typeof(ARAI_MappingTable)) as ARAI_MappingTable;
                }
            }
            return _instance;
        }
    }

    public List<Phoneme> phonemeMapper = new List<Phoneme>()
    {
        new Phoneme('ㅏ' , "AI"         , 1.00f),
        new Phoneme('ㅐ' , "E"          , 1.00f),
        new Phoneme('ㅑ' , "AI"         , 1.00f),
        new Phoneme('ㅒ' , "E"          , 1.00f),
        new Phoneme('ㅓ' , "U"          , 1.00f),
        new Phoneme('ㅔ' , "E"          , 1.00f),
        new Phoneme('ㅖ' , "E"          , 1.00f),
        new Phoneme('ㅗ' , "O"          , 1.00f),
        new Phoneme('ㅘ' , "AI"         , 1.00f),
        new Phoneme('ㅙ' , "E"          , 1.00f),
        new Phoneme('ㅚ' , "E"          , 1.00f),
        new Phoneme('ㅛ' , "O"          , 1.00f),
        new Phoneme('ㅜ' , "O"          , 1.00f),
        new Phoneme('ㅝ' , "U"          , 1.00f),
        new Phoneme('ㅞ' , "U"          , 1.00f),
        new Phoneme('ㅟ' , "AI"         , 1.00f),
        new Phoneme('ㅠ' , "U"          , 1.00f),
        new Phoneme('ㅡ' , "U"          , 1.00f),
        new Phoneme('ㅢ' , "U"          , 1.00f),
        new Phoneme('ㅣ' , "AI"         , 1.00f),
        
		new Phoneme('ㄱ' , "CDGKNRSThYZ", 1.00f),
        new Phoneme('ㄲ' , "CDGKNRSThYZ", 1.00f),
        new Phoneme('ㄴ' , "E"          , 1.00f),
        new Phoneme('ㄷ' , "E"          , 1.00f),
        new Phoneme('ㄸ' , "E"          , 1.00f),
        new Phoneme('ㄹ' , "E"          , 1.00f),
        new Phoneme('ㅁ' , "MBP"        , 1.00f),
        new Phoneme('ㅂ' , "MBP"        , 1.00f),
        new Phoneme('ㅃ' , "MBP"        , 1.00f),
        new Phoneme('ㅅ' , "CDGKNRSThYZ", 1.00f),
        new Phoneme('ㅆ' , "CDGKNRSThYZ", 1.00f),
        new Phoneme('ㅇ' , "AI"         , 1.00f),
        new Phoneme('ㅈ' , "CDGKNRSThYZ", 1.00f),
        new Phoneme('ㅉ' , "CDGKNRSThYZ", 1.00f),
        new Phoneme('ㅊ' , "MBP"        , 1.00f),
        new Phoneme('ㅋ' , "CDGKNRSThYZ", 1.00f),
        new Phoneme('ㅌ' , "CDGKNRSThYZ", 1.00f),
        new Phoneme('ㅍ' , "FV"         , 1.00f),
        new Phoneme('ㅎ' , "CDGKNRSThYZ", 1.00f),
        new Phoneme('ㄳ' , "FV"         , 1.00f),
        new Phoneme('ㄵ' , "FV"         , 1.00f),
        new Phoneme('ㄶ' , "CDGKNRSThYZ", 1.00f),
        new Phoneme('ㄺ' , "CDGKNRSThYZ", 1.00f),
        new Phoneme('ㄻ' , "CDGKNRSThYZ", 1.00f),
        new Phoneme('ㄼ' , "CDGKNRSThYZ", 1.00f),
        new Phoneme('ㄽ' , "CDGKNRSThYZ", 1.00f),
        new Phoneme('ㄾ' , "CDGKNRSThYZ", 1.00f),
        new Phoneme('ㄿ' , "CDGKNRSThYZ", 1.00f),
        new Phoneme('ㅀ' , "CDGKNRSThYZ", 1.00f),
        new Phoneme('ㅄ' , "CDGKNRSThYZ", 1.00f),

        new Phoneme('a'  , "AI"         , 1.00f),
        new Phoneme('b'  , "MBP"        , 1.00f),
        new Phoneme('c'  , "CDGKNRSThYZ", 1.00f),
        new Phoneme('d'  , "CDGKNRSThYZ", 1.00f),
        new Phoneme('e'  , "E"          , 1.00f),
        new Phoneme('f'  , "FV"         , 1.00f),
        new Phoneme('g'  , "CDGKNRSThYZ", 1.00f),
        new Phoneme('h'  , "CDGKNRSThYZ", 1.00f),
        new Phoneme('i'  , "AI"         , 1.00f),
        new Phoneme('j'  , "CDGKNRSThYZ", 1.00f),
        new Phoneme('k'  , "CDGKNRSThYZ", 1.00f),
        new Phoneme('l'  , "U"          , 0.75f),
        new Phoneme('m'  , "MBP"        , 1.00f),
        new Phoneme('n'  , "CDGKNRSThYZ", 1.00f),
        new Phoneme('o'  , "O"          , 1.00f),
        new Phoneme('p'  , "MBP"        , 1.00f),
        new Phoneme('q'  , "O"          , 1.00f),
        new Phoneme('r'  , "CDGKNRSThYZ", 1.00f),
        new Phoneme('s'  , "CDGKNRSThYZ", 1.00f),
        new Phoneme('t'  , "CDGKNRSThYZ", 1.00f),
        new Phoneme('u'  , "U"          , 1.00f),
        new Phoneme('v'  , "FV"         , 1.00f),
        new Phoneme('w'  , "O"          , 1.00f),
        new Phoneme('x'  , "AI"         , 1.00f),
        new Phoneme('y'  , "CDGKNRSThYZ", 1.00f),
        new Phoneme('z'  , "CDGKNRSThYZ", 1.00f),
                        
        new Phoneme('ɑ'  , "AI"         , 0.75f),
        new Phoneme('ɪ'  , "AI"         , 0.33f),
        new Phoneme('ə'  , "O"          , 1.00f),
        new Phoneme('ð' , "CDGKNRSThYZ", 0.75f),
        new Phoneme('ʌ'  , "AI"         , 0.50f),
        new Phoneme('ŋ' , "CDGKNRSThYZ", 0.75f),
        new Phoneme('ʤ'  , "CDGKNRSThYZ", 0.50f),
        new Phoneme('ʒ'  , "CDGKNRSThYZ", 0.33f),
        new Phoneme('ʧ'  , "CDGKNRSThYZ", 0.75f),
        new Phoneme('ʃ'  , "CDGKNRSThYZ", 0.75f),
        new Phoneme('θ' , "CDGKNRSThYZ", 1.00f),
        new Phoneme('ɛ'  , "E"          , 0.50f),
        new Phoneme('ɔ'  , "O"          , 1.00f),
        new Phoneme('æ' , "E"          , 0.75f),
        new Phoneme('ː' , " "          , 1.00f),
        new Phoneme('ˈ'  , " "          , 1.00f),
    };

    public List<Emotion> emotionMapper = new List<Emotion>()
    {
        new Emotion("Hate"      , new string[]{ "hate" }), 
        new Emotion("Happy"     , new string[]{ "happy" }), 
        new Emotion("Angry"     , new string[]{ "angry" }), 
        new Emotion("Sad"       , new string[]{ "sad" }), 
        new Emotion("Surprisal" , new string[]{ "surprisal" }), 
    };
};

[System.Serializable]
public class Phoneme
{
    public char character = ' ';
    public string phoneme = "";
    public float weight = 1.0f;



    public Phoneme(string _phoneme)
    {
        character = ' ';
        phoneme = _phoneme;
        weight = 1.0f;
    }

    public Phoneme(string _phoneme, float _weight)
    {
        character = ' ';
        phoneme = _phoneme;
        weight = _weight;        
    }

    public Phoneme(char _character, string _phoneme, float _weight)
    {
        character = _character;
        phoneme = _phoneme;
        weight = _weight;
    }
}

[System.Serializable]
public class Emotion
{
    public string emotionName = "";
    public List<string> texts;



    public Emotion(string _emotionName)
    {
        emotionName = _emotionName;
        texts = new List<string>();
    }

    public Emotion(string _emotionName, string[] _texts)
    {
        emotionName = _emotionName;
        texts = new List<string>(_texts);
    }
}

[System.Serializable]
public class TalkAction
{
    public string actionName = "";
    public List<string> texts;



    public TalkAction(string _actionName)
    {
        actionName = _actionName;
        texts = new List<string>();
    }

    public TalkAction(string _actionName, string[] _texts)
    {
        actionName = _actionName;
        texts = new List<string>(_texts);
    }
}