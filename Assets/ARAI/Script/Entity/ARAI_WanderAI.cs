﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARAI_WanderAI : MonoBehaviour
{
    /// <summary>
    /// WanderAI의 동작 상태.
    /// </summary>
    public bool wanderEnabled = true;
    /// <summary>
    /// AI 종류, 에디터에서 수정.
    /// </summary>
    public AIType aiType = AIType.Ground;
    /// <summary>
    /// 이동 기준점, 우주인에게는 공전할 기준점.
    /// </summary>
    public Vector3 centerPos;
    /// <summary>
    /// 이동 제한거리, 우주인에게는 기준점으로부터의 거리.
    /// </summary>
    public float wanderingRange = 1f;
    /// <summary>
    /// 전체적인 속도 조절.
    /// </summary>
    public float speed = 1f;
    /// <summary>
    /// 아바타 모델.
    /// </summary>
    public Transform model;
    /// <summary>
    /// 이동방향 변경 시 각도 오차, 우주인에게는 공전 시 각도 오차.
    /// </summary>
    public float angleError = 20f;
    /// <summary>
    /// 정지상태에서 움직임을 시도해보는 간격.
    /// </summary>
    public float wanderThinkInterval = 2f;
    /// <summary>
    /// 움직임을 시도할 때, 움직임이 발생할 확률.
    /// </summary>
    [Range(0f, 1f)]
    public float wanderAppearChance = 0.33f;
    /// <summary>
    /// 움직임이 지속되는 시간.
    /// </summary>
    public float wanderDuration = 1f;
    /// <summary>
    /// 캐릭터가 방황하는 중인지의 여부.
    /// </summary>
    public bool wandering = true;
    /// <summary>
    /// 디버깅 옵션. 씬에 라인 등을 출력.
    /// </summary>
    public bool debugVisible = true;
        
    private Transform target;
    private Animator animator;
    private Coroutine aiCoroutine;
    private Coroutine controlCoroutine;
    private ARAI_ClickToMove clickToMove;



    private void Start()
    {
        target = this.transform;
        clickToMove = this.GetComponent<ARAI_ClickToMove>();
        animator = this.GetComponent<ARAI_IntelligenEntity>().animator;

        switch (aiType)
        {
            case AIType.Ground:
                aiCoroutine = StartCoroutine(GroundWander());
                break;
            case AIType.Air:
                aiCoroutine = StartCoroutine(AirWander());
                break;
            case AIType.Astronaut:
                aiCoroutine = StartCoroutine(AstronautWander());
                break;
            default:
                break;
        }
    }

    private IEnumerator AstronautWander()
    {
        wandering = true;

        Vector3 current = target.position;
        Vector3 angle = target.eulerAngles;
        Vector3 direction = new Vector3();
        Vector3 worldDirection;
        float timer = 0f;
        float angleTimer = 0f;
        float radian = 0f;

        angle.x = -angleError;

        while (true)
        {
            if (!wanderEnabled)
            {
                yield return null;
                continue;
            }
            
            timer = Mathf.Repeat(timer + Time.deltaTime * speed, 5f);
            radian = 360f * (timer / 5f) * Mathf.Deg2Rad;
            direction.x = Mathf.Cos(radian);
            direction.y = Mathf.Sin(radian);
            worldDirection = target.TransformVector(direction);

            current = centerPos + worldDirection * wanderingRange;
            target.transform.position = current;

            angleTimer = (angleTimer + Time.deltaTime * speed) % 20f;
            angle.x = Mathf.LerpAngle(-angleError, angleError, Mathf.PingPong(angleTimer, 10f) / 10f);
            target.eulerAngles = angle;
            
            model.rotation = Quaternion.LookRotation((centerPos - model.position).normalized, model.up);
            yield return null;
        }
    }

    private IEnumerator GroundWander()
    {
        controlCoroutine = StartCoroutine(WanderControl());
        
        float goalDegree = Random.Range(0f, 360f);
        Vector3 direction = new Vector3(Mathf.Cos(goalDegree), 0f, Mathf.Sin(goalDegree));
        Vector3 toCenter = (centerPos - target.position).normalized;
        Vector3 destDirection = (direction - toCenter).normalized;
        Vector3 destination = centerPos + destDirection * wanderingRange;
        Vector3 eulerAngle = target.eulerAngles;
        float distance = Vector3.Distance(target.position, destination);

        if (debugVisible)
        {
            Debug.DrawRay(centerPos, new Vector3( 1f, 0f,  0f).normalized * wanderingRange, Color.magenta, 1000f);
            Debug.DrawRay(centerPos, new Vector3( 1f, 0f,  1f).normalized * wanderingRange, Color.magenta, 1000f);
            Debug.DrawRay(centerPos, new Vector3( 0f, 0f,  1f).normalized * wanderingRange, Color.magenta, 1000f);
            Debug.DrawRay(centerPos, new Vector3(-1f, 0f,  1f).normalized * wanderingRange, Color.magenta, 1000f);
            Debug.DrawRay(centerPos, new Vector3(-1f, 0f,  0f).normalized * wanderingRange, Color.magenta, 1000f);
            Debug.DrawRay(centerPos, new Vector3(-1f, 0f, -1f).normalized * wanderingRange, Color.magenta, 1000f);
            Debug.DrawRay(centerPos, new Vector3( 0f, 0f, -1f).normalized * wanderingRange, Color.magenta, 1000f);
            Debug.DrawRay(centerPos, new Vector3( 1f, 0f, -1f).normalized * wanderingRange, Color.magenta, 1000f);
        }

        while (true)
        {
            if (!wanderEnabled)
            {
                yield return null;
                continue;
            }
            else if (!wandering)
            {
				if (!clickToMove.isMoving)
				{
					direction = Camera.main.transform.position - target.position;
					direction.y = 0;
					direction.Normalize();
					Quaternion targetRotation = Quaternion.LookRotation(direction);
					this.transform.rotation = Quaternion.RotateTowards(this.transform.rotation, targetRotation, 105f * Time.deltaTime);
				}
				yield return null;
                continue;
            }
            
            eulerAngle = target.eulerAngles;
            toCenter = (centerPos - target.position).normalized;
            direction = (destination - target.position).normalized;
            goalDegree = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
            distance = Vector3.Distance(target.position, destination);
            
            if (Mathf.Abs(Mathf.DeltaAngle(eulerAngle.y, goalDegree)) >= 1f)
            {
                eulerAngle.y = Mathf.MoveTowardsAngle(eulerAngle.y, goalDegree, 105f * Time.deltaTime * speed);
                target.eulerAngles = eulerAngle;
            }
            else if (distance >= 0.05f)
            {
                target.Translate(Vector3.forward * 0.3f * Time.deltaTime * speed);
            }
            else
            {
                toCenter = (centerPos - target.position).normalized;
                goalDegree = Mathf.Atan2(toCenter.x, toCenter.z) * Mathf.Rad2Deg + Random.Range(-angleError, angleError);
                direction = new Vector3(Mathf.Sin(goalDegree * Mathf.Deg2Rad), 0f, Mathf.Cos(goalDegree * Mathf.Deg2Rad));
                destDirection = (direction * wanderingRange - toCenter * wanderingRange * 0.5f).normalized;
                destination = centerPos + destDirection * wanderingRange;

                if (debugVisible)
                {
                    Debug.DrawRay(target.position, toCenter * 10f, Color.black, 4f);
                    Debug.DrawRay(centerPos, destDirection * 10f, Color.gray, 4f);
                    Debug.DrawRay(target.position, direction * 10f, Color.cyan, 4f);
                }
            }
            
            if (debugVisible)
            {
                Debug.DrawRay(destination, Vector3.up * 10f, Color.yellow, Time.deltaTime);
                Debug.DrawRay(target.position, direction * 10f, Color.blue, Time.deltaTime);
            }
            
            yield return null;
        }
    }

    private IEnumerator AirWander()
    {
        float goalDegree = Random.Range(0f, 360f);
        Vector3 direction = new Vector3(Mathf.Sin(goalDegree), 0f, Mathf.Cos(goalDegree));
        Vector3 toCenter = (centerPos - target.position).normalized;
        Vector3 destDirection = (direction - toCenter).normalized;
        Vector3 destination = centerPos + destDirection * wanderingRange;
        Vector3 eulerAngle = target.eulerAngles;
        float distance = Vector3.Distance(target.position, destination);

        if (debugVisible)
        {
            Debug.DrawRay(centerPos, new Vector3( 1f, 0f,  0f).normalized * wanderingRange, Color.magenta, 1000f);
            Debug.DrawRay(centerPos, new Vector3( 1f, 0f,  1f).normalized * wanderingRange, Color.magenta, 1000f);
            Debug.DrawRay(centerPos, new Vector3( 0f, 0f,  1f).normalized * wanderingRange, Color.magenta, 1000f);
            Debug.DrawRay(centerPos, new Vector3(-1f, 0f,  1f).normalized * wanderingRange, Color.magenta, 1000f);
            Debug.DrawRay(centerPos, new Vector3(-1f, 0f,  0f).normalized * wanderingRange, Color.magenta, 1000f);
            Debug.DrawRay(centerPos, new Vector3(-1f, 0f, -1f).normalized * wanderingRange, Color.magenta, 1000f);
            Debug.DrawRay(centerPos, new Vector3( 0f, 0f, -1f).normalized * wanderingRange, Color.magenta, 1000f);
            Debug.DrawRay(centerPos, new Vector3( 1f, 0f, -1f).normalized * wanderingRange, Color.magenta, 1000f);
        }

        while (true)
        {
            if (!wanderEnabled)
            {
                yield return null;
                continue;
            }
            else if (!wandering)
            {
				if (!clickToMove.isMoving)
				{
					direction = Camera.main.transform.position - target.position;
					direction.y = 0;
					direction.Normalize();
					Quaternion targetRotation = Quaternion.LookRotation(direction);
					this.transform.rotation = Quaternion.RotateTowards(this.transform.rotation, targetRotation, 105f * Time.deltaTime);
				}
                yield return null;
                continue;
            }

            eulerAngle = target.eulerAngles;
            toCenter = (centerPos - target.position).normalized;
            direction = (destination - target.position).normalized;
            goalDegree = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
            distance = Vector3.Distance(target.position, destination);

            if (Mathf.Abs(Mathf.DeltaAngle(eulerAngle.y, goalDegree)) >= 1f)
            {
                eulerAngle.y = Mathf.MoveTowardsAngle(eulerAngle.y, goalDegree, 105f * Time.deltaTime * speed);
                target.eulerAngles = eulerAngle;
            }
            else if (distance >= 0.05f)
            {
                target.Translate(Vector3.forward * 0.3f * Time.deltaTime * speed);
            }
            else
            {
                toCenter = (centerPos - target.position).normalized;
                goalDegree = Mathf.Atan2(toCenter.x, toCenter.z) * Mathf.Rad2Deg + Random.Range(-angleError, angleError);
                direction = new Vector3(Mathf.Sin(goalDegree * Mathf.Deg2Rad), 0f, Mathf.Cos(goalDegree * Mathf.Deg2Rad));
                destDirection = (direction * wanderingRange - toCenter * wanderingRange * 0.5f).normalized;
                destination = centerPos + destDirection * wanderingRange;

                if (debugVisible)
                {
                    Debug.DrawRay(target.position, toCenter * 10f, Color.black, 4f);
                    Debug.DrawRay(centerPos, destDirection * 10f, Color.gray, 4f);
                    Debug.DrawRay(target.position, direction * 10f, Color.cyan, 4f);
                }
            }

            if (debugVisible)
            {
                Debug.DrawRay(destination, Vector3.up * 10f, Color.yellow, Time.deltaTime);
                Debug.DrawRay(target.position, direction * 10f, Color.blue, Time.deltaTime);
            }

            yield return null;
        }
    }

    private IEnumerator WanderControl()
    {
        float timer = 0f;
        float chance = 0f;
        float interval = wanderThinkInterval;

        while (true)
        {
            while (timer < interval)
            {
                if (wanderEnabled && !clickToMove.isMoving)
                    timer += Time.deltaTime;

                if (clickToMove.isMoving)
                    wandering = false;

                yield return null;
            }
            timer = 0f;

            interval = wanderThinkInterval;
            wandering = false;

            chance = Random.Range(0f, 1f);
            if (chance <= wanderAppearChance)
            {
                wandering = true;
                interval = wanderDuration;
                if (animator && aiType == AIType.Ground)
                    animator.SetBool("Walking", true);
            }
            else if (animator && aiType == AIType.Ground)
                animator.SetBool("Walking", false);
        }
    }



    public enum AIType
    {
        Ground = 0,
        Air = 1,
        Astronaut = 2,
    }
}
