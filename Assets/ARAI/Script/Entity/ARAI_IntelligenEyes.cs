﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARAI_IntelligenEyes : MonoBehaviour
{
    public int blinkShapeIndex;
    public float blinkDuration = 0.2f;
    public float blinkInterval = 5.0f;
    [HideInInspector]
    public SkinnedMeshRenderer skinnedMesh;

    [Range(0,1)]
    public float randomIntervalTime = 0.3f;

    

    private void Start()
    {
        StartCoroutine(BlinkEyeUpdate());
    }
    
    private IEnumerator BlinkEyeUpdate()
    {
        while (true)
        {
            float randomInterval = blinkInterval + Random.Range(-blinkInterval * randomIntervalTime, blinkInterval * randomIntervalTime);
            yield return new WaitForSeconds(randomInterval);
            SetBlink(blinkShapeIndex, blinkDuration);
        }
    }

    private void SetBlink(int BlinkShapeIndex, float duration)
    {
        StartCoroutine(SetBlinkSmooth(BlinkShapeIndex, duration));
    }

    private IEnumerator SetBlinkSmooth(int BlinkShapeIndex, float duration)
    {
        float blendTime = 0;
        bool blendFinished = false;
        while (blendTime <= duration)
        {
            blendTime += Time.deltaTime;
            skinnedMesh.SetBlendShapeWeight(BlinkShapeIndex, (blendTime / duration) * 100.0f);
            yield return null;
        }
        while (!blendFinished)
        {
            blendTime -= Time.deltaTime;
            skinnedMesh.SetBlendShapeWeight(BlinkShapeIndex, (blendTime / duration) * 100.0f);
            if (blendTime <= 0)
            {
                blendFinished = true;
            }
            yield return null;
        }
    }
}
