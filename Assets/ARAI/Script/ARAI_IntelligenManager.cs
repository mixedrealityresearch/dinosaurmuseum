﻿using HoloToolkit.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ARAI_IntelligenManager : MonoBehaviour
{
    public QuestionAnswerDictionary questions;

    //=======Public========//
    /// <summary>
    /// The IntelligentEntity that is now working.
    /// </summary>
    [HideInInspector]
    public ARAI_IntelligenEntity intelligentEntity;
    /// <summary>
    /// All of IntelligentEntity be stored in this. 
    /// </summary>
    [HideInInspector]
    public List<ARAI_IntelligenEntity> intelligentEntitys;
    /// <summary>
    /// The Scenario that is now playing.
    /// </summary>
    [HideInInspector]
    public ARAI_Scenario scenario;

    //=======Callback========//
    public delegate void PlayTalkSoundHandler();
    public static event PlayTalkSoundHandler FinishPhonemeAnalysis;

    [HideInInspector]
    public string answer;

    private void Awake()
    {
        // Set DontDestroyOnLoad to this manager.
        DontDestroyOnLoad(this);

        // Find all IntelligentEntitys.
        intelligentEntitys = new List<ARAI_IntelligenEntity>(GameObject.FindObjectsOfType<ARAI_IntelligenEntity>());
        intelligentEntity = intelligentEntitys[0];
    }

    private void Start ()
    {
        ARAI_IntelligenEntity.OnFinishPhonemeAnalysis += this.PlaySound;
    }

    private void OnApplicationQuit()
    {
        ARAI_IntelligenEntity.OnFinishPhonemeAnalysis -= this.PlaySound;
    }
    
    /// <summary>
    /// Play Sound This.
    /// </summary>
    public void PlaySound()
    {
        Debug.Log("PlaySound with: " + answer);
    }

    /// <summary>
    /// Init TTS String</para>
    /// ex  :  IntelligentTalk( string("Hello nice to meet you"));
    /// </summary>
    /// <param name="_intelligentTalk"></param>
    public void IntelligentTalk(string _intelligentTalk)
    {
        if (_intelligentTalk.Length == 0)
            return;

        if (!intelligentEntity)
        {
            intelligentEntity = GetComponent<ARAI_IntelligenEntity>();
        }
        intelligentEntity.IntelligentTalk(_intelligentTalk);
    }

    public void intelligentTalkGazedObject(GameObject gazedDinoGameObject, string __intelligentTalk)
    {
        if (__intelligentTalk.Length == 0) return;

        string[] st = new string[2];
        ARAI_IntelligenEntity intelligentEntity = gazedDinoGameObject.GetComponent<ARAI_IntelligenEntity>();
        if (intelligentEntity)
        {
            st = questions.GetAnswers(__intelligentTalk);
            __intelligentTalk = st[0];
            answer = __intelligentTalk;
            intelligentEntity.IntelligentTalk(__intelligentTalk);
            if (st[1] != null)
            {
                intelligentEntity.animator.Play(st[1], -1, 0f);
            }
        }
    }

    public void intelligentSelectTalkGazedObject(GameObject gazedDinoGameObject, string __intelligentTalk)
    {
        if (__intelligentTalk.Length == 0) return;

        //string[] st = new string[2];
        ARAI_IntelligenEntity intelligentEntity = gazedDinoGameObject.GetComponent<ARAI_IntelligenEntity>();
        if (intelligentEntity)
        {
            answer = __intelligentTalk;
            intelligentEntity.IntelligentTalk(__intelligentTalk);
            intelligentEntity.animator.Play("Happy", -1, 0f);
        }
    }

    /// <summary>
    /// Init Click In hololens Gaze Position
    /// </summary>
    /// <param name="_Position"></param>
    public void MoveToPosition(Vector3 _position)
    {
        if(null != intelligentEntity)
        {
            if (intelligentEntity.clickToMove != null)
                intelligentEntity.clickToMove.MoveToPosition(_position);
        }
    }

    public void MoveToPositionGazedObject(GameObject gazedDinoGameObject, Vector3 _position)
    {
        ARAI_IntelligenEntity intelligentEntity = gazedDinoGameObject.GetComponent<ARAI_IntelligenEntity>();
        if (intelligentEntity)
        {
            intelligentEntity.clickToMove.MoveToPosition(_position);
        }
    }

    /// <summary>
    /// Play emotion for IntelligenEntity.
    /// </summary>
    /// <param name="_emotionName">The name of emotion to play.</param>
    /// <param name="_intensity">Emotion's intensity. 0.0 ~ 1.0</param>
    /// <param name="_duration">Emotion's duration, Normal is 3 sec.</param>
    public void PlayEmotion(string _emotionName, float _intensity, float _duration = 3.0f)
    {
        if (null != intelligentEntity)
        {
            intelligentEntity.SetEmotion(_emotionName, _intensity, _duration);
        }
    }

    /// <summary>
    /// Reset all emotions to initial state for IntelligenEntity.
    /// </summary>
    public void ResetEmotion()
    {
        if (null != intelligentEntity)
        {
            intelligentEntity.ResetEmotion();
        }
    }

    /// <summary>
    /// Set the IntelligentEntity.
    /// </summary>
    /// <param name="_entity"></param>
    public void SetIntelligentEntity(ARAI_IntelligenEntity _entity)
    {
        intelligentEntity = _entity;
    }




    //=============== Not Use===========//
    /// <summary>
    /// Start new scenario scene.
    /// </summary>
    /// <param name="_scenarioName">The name of scenario to play. (Scene's name)</param>
    public void StartScenario(string _scenarioName)
    {
        SceneManager.LoadScene(_scenarioName);
    }

    /// <summary>
    /// Go to MainScene.
    /// </summary>
    public void GoMainScene()
    {
        SceneManager.LoadScene("MainScene");
    }

    /// <summary>
    /// Change the current IntelligentEntity to ohter entity in IntelligentEntitys. FOR TEST
    /// </summary>
    /// <param name="_index"></param>
    public void SwapIntelligentEntity(int _index)
    {
        intelligentEntity = intelligentEntitys[_index];
    }

}
